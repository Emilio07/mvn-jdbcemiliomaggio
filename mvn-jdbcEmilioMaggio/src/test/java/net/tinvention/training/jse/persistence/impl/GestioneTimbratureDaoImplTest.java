package net.tinvention.training.jse.persistence.impl;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import net.tinvention.training.jse.dto.UtenteDto;

class GestioneTimbratureDaoImplTest {

	private GestioneTimbratureDaoImpl gestioneTimbratureDaoImpl = new GestioneTimbratureDaoImpl(
			"src\\test\\resources\\test.properties");
	private UtenteDto u;

	@BeforeEach
	public void init() throws Exception {
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con
						.prepareStatement("insert into utenti (matricola,nome,cognome) VALUES (?,?,?);");) {
			prep.setInt(1, 1);
			prep.setString(2, "Mario");
			prep.setString(3, "Rossi");
			prep.executeUpdate();
			u = new UtenteDto(2, "Emilio", "Maggio");
		}
	}

	@Test
	void aggiungiRigaTabellaDatabaseTest() throws SQLException {
		gestioneTimbratureDaoImpl.aggiungiRigaTabellaDatabase(u);
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("select* from utenti;");) {
			ResultSet res = prep.executeQuery();
			Integer i = 0;
			while (res.next()) {
				i = res.getInt(1);
			}
			assertTrue(i == 2);

		}
	}

	@Test
	void eliminaRigaTabellaDatabaseTest() throws SQLException {
		gestioneTimbratureDaoImpl.eliminaRigaTabellaDatabase(1);
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("select* from utenti where matricola=1;");) {
			ResultSet res = prep.executeQuery();
			Integer i = null;
			if (res.next()) {
				i = res.getInt(1);
			}
			assertNull(i);

		}
	}

	@Test
	void aggiornaTimbratureTabellaDatabaseTest() throws SQLException {
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("select* from utenti where matricola=1;");) {
			ResultSet res = prep.executeQuery();
			gestioneTimbratureDaoImpl.eliminaRigaTabellaDatabase(1);
			Integer i = 0;
			if (res.next()) {
				i = res.getInt(1);
			}
			assertTrue(i > 0);
		}
	}

	@Test
	void cercaUtenteTabellaDatabaseTest() throws Exception {
		Object utente = gestioneTimbratureDaoImpl.cercaUtenteTabellaDatabase(1);
		assertAll("utente", () -> assertNotNull(utente), () -> assertTrue(utente instanceof UtenteDto),
				() -> assertTrue(((UtenteDto) utente).getMatricola() == 1));

	}

	@Test
	void restituisciListaUtentiTabellaDatabaseTest() throws Exception {
		List<UtenteDto> lista1 = new ArrayList<UtenteDto>();
		lista1.add(new UtenteDto(1, "Mario", "Rossi"));
		assertTrue(gestioneTimbratureDaoImpl.restituisciListaUtentiTabellaDatabase() instanceof List<?>);
		assertEquals(gestioneTimbratureDaoImpl.restituisciListaUtentiTabellaDatabase().get(0).getMatricola(), 1);
	}

	@AfterEach
	public void trillDown() throws SQLException {
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("delete from utenti where matricola=1 or matricola=2");) {
			prep.executeUpdate();
		}
		u = null;
	}
}
