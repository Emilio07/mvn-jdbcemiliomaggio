package net.tinvention.training.jse.applation.ui;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import net.tinvention.training.jse.application.ui.ApplicationUI;
import net.tinvention.training.jse.bl.impl.GestioneTimbratureLogicImpl;
import net.tinvention.training.jse.dto.UtenteDto;
import net.tinvention.training.jse.persistence.impl.GestioneTimbratureDaoImpl;

class ApplicationUITest {
	private static ApplicationUI applicationUi;
	private static GestioneTimbratureLogicImpl gestioneTimbratureLogicImpl;
	private static GestioneTimbratureDaoImpl gestioneTimbratureDaoImpl = new GestioneTimbratureDaoImpl(
			"src\\test\\resources\\test.properties");
	private static UtenteDto u;
	private static UtenteDto u2;
	List<UtenteDto> l;

	private final static ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final PrintStream originalOut = System.out;

	@BeforeAll
	public static void initAll() {
		applicationUi = new ApplicationUI();
		gestioneTimbratureLogicImpl = new GestioneTimbratureLogicImpl();
		gestioneTimbratureLogicImpl.setGestioneTimbratureDao(gestioneTimbratureDaoImpl);
		applicationUi.setGestioneTimbratureLogic(gestioneTimbratureLogicImpl);
		System.setOut(new PrintStream(outContent));
	}

	@BeforeEach
	public void init() throws Exception {
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con
						.prepareStatement("insert into utenti(matricola,nome,cognome) VALUES (?,?,?);");) {
			prep.setInt(1, 1);
			prep.setString(2, "Mario");
			prep.setString(3, "Rossi");
			prep.executeUpdate();
		}
		u = new UtenteDto(2, "Emilio", "Maggio", 0);
		u2 = new UtenteDto(1, "Mario", "Rossi", 0);
		l = new ArrayList<UtenteDto>();
	}

	@Test
	void addTest() throws Exception {
		applicationUi.add(2, "Emilio", "Maggio");
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("select* from utenti");) {
			ResultSet res = prep.executeQuery();
			Integer i = 0;
			while (res.next()) {
				i = res.getInt(1);
			}
			;
			assertTrue(i == 2);
		}
	}

	@Test
	void deleteTest() throws Exception {
		applicationUi.delete(1);
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("select* from utenti where matricola=1;");) {
			ResultSet res = prep.executeQuery();
			Integer i = null;
			if (res.next()) {
				i = res.getInt(1);
			}
			assertNull(i);

		}
	}

	@Test
	void timbraUtentiTest() throws Exception {
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("select* from utenti where matricola=1;");) {
			ResultSet res = prep.executeQuery();
			applicationUi.timbra(1);
			Integer i = 0;
			if (res.next()) {
				i = res.getInt(1);
			}
			assertTrue(i > 0);
		}
	}

	@Test
	public void printAllTest() throws NullPointerException, IOException, SQLException {
		l.add(u);
		l.add(u2);
		applicationUi.printAll(l);
		assertEquals("2 Emilio Maggio 0\r\n" + "1 Mario Rossi 0", outContent.toString().trim());
	}

	@AfterEach
	public void trillDown() throws Exception {
		try (Connection con = DriverManager.getConnection(gestioneTimbratureDaoImpl.getJDBCURL(),
				gestioneTimbratureDaoImpl.getUSERNAME(), gestioneTimbratureDaoImpl.getPASSWORD());
				PreparedStatement prep = con.prepareStatement("delete from utenti where matricola=1 or matricola=2");) {
			prep.executeUpdate();
		}
		u = null;
		System.setOut(originalOut);
	}

}
