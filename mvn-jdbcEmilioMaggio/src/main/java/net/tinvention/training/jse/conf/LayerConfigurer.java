package net.tinvention.training.jse.conf;

import net.tinvention.training.jse.bl.impl.GestioneTimbratureLogicImpl;
import net.tinvention.training.jse.persistence.impl.GestioneTimbratureDaoImpl;
import net.tinvention.training.jse.application.ui.ApplicationUI;

/* This class configures the dependencies, injecting appropriate instances
* This class take control on what instances inject , configuring the layers dependencies 
* @author Emilio*/

public class LayerConfigurer {

	public static ApplicationUI getApplicaitonConfigured() {

		GestioneTimbratureDaoImpl gestioneTimbratureDao = new GestioneTimbratureDaoImpl(
				"src\\main\\resources\\main.properties");

		GestioneTimbratureLogicImpl gestioneTimbratureLogic = new GestioneTimbratureLogicImpl();
		gestioneTimbratureLogic.setGestioneTimbratureDao(gestioneTimbratureDao);

		ApplicationUI result = new ApplicationUI();
		result.setGestioneTimbratureLogic(gestioneTimbratureLogic);

		return result;

	}

}
