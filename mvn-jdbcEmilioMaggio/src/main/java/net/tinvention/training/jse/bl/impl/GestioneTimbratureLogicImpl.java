package net.tinvention.training.jse.bl.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.tinvention.training.jse.bl.GestioneTimbratureLogic;
import net.tinvention.training.jse.dto.UtenteDto;
import net.tinvention.training.jse.persistence.GestioneTimbratureDao;

public class GestioneTimbratureLogicImpl implements GestioneTimbratureLogic {
	private GestioneTimbratureDao gestioneTimbratureDao;
	private Logger LOG = LogManager.getLogger(GestioneTimbratureLogicImpl.class);

	@Override
	public void aggiungiRigaTabellaUtenti(UtenteDto u) throws SQLException {
		if (gestioneTimbratureDao.cercaUtenteTabellaDatabase(u.getMatricola()) == null) {
			gestioneTimbratureDao.aggiungiRigaTabellaDatabase(u);
		}
	}

	@Override
	public void eliminaRigaTabellaUtenti(int numMatricola) throws SQLException {
		LOG.debug("metodo aggiungiRigaTabellaUtenti richiamato a livello BLogic");
		if (gestioneTimbratureDao.cercaUtenteTabellaDatabase(numMatricola) != null) {
			gestioneTimbratureDao.eliminaRigaTabellaDatabase(numMatricola);
			;
		}
	}

	@Override
	public void aggiornaTimbratureTabellaUtenti(int numMatricola) throws SQLException {
		if (gestioneTimbratureDao.cercaUtenteTabellaDatabase(numMatricola) != null) {
			gestioneTimbratureDao.aggiornaTimbratureTabellaDatabase(numMatricola);
		}
	}

	@Override
	public UtenteDto ottieniUtenteTabellaUtenti(int numMatricola) throws SQLException {
		return gestioneTimbratureDao.cercaUtenteTabellaDatabase(numMatricola);
	}

	@Override
	public List<UtenteDto> restituisciListaUtentiTabellaUtenti() throws SQLException {
		return gestioneTimbratureDao.restituisciListaUtentiTabellaDatabase();

	}

	public GestioneTimbratureDao getGestioneRegistroDao() {
		return gestioneTimbratureDao;
	}

	public void setGestioneTimbratureDao(GestioneTimbratureDao gestioneRegistroDao) {
		this.gestioneTimbratureDao = gestioneRegistroDao;
	}
}