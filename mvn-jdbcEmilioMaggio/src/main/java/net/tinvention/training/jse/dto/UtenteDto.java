package net.tinvention.training.jse.dto;

public class UtenteDto {
	private int matricola;
	private String nome;
	private String cognome;
	private int timbrature;

	public UtenteDto() {
	};

	public UtenteDto(int matricola, String nome, String cognome) {
		this.matricola = matricola;
		this.nome = nome;
		this.cognome = cognome;
	}

	public UtenteDto(int matricola, String nome, String cognome, int timbrature) {
		this.matricola = matricola;
		this.nome = nome;
		this.cognome = cognome;
		this.timbrature = timbrature;
	}

	public int getMatricola() {
		return matricola;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public int getTimbrature() {
		return timbrature;
	}

	public void setMatricola(int matricola) {
		this.matricola = matricola;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setTimbrature(int timbrature) {
		this.timbrature = timbrature;
	}

	public String toString(UtenteDto u) {
		return u.getMatricola() + " " + u.getNome() + " " + u.getCognome() + " " + u.getTimbrature();
	}
}
